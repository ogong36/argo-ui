import axios from 'axios';
// import { Role } from '../users/components/models';
import {endpoint} from '../../devstack/classes/constants';
// const keystoneEndPoint = 'http://183.111.177.141/identity/v3';
const accessToken = localStorage.getItem('accessToken');
const headers = {
   Authorization: `Bearer ${accessToken}` 
}
export class RolesService {
    public async get(): Promise<any> {
      const response = await axios.get(`${endpoint}/role`, { headers });
      return response.data;
  };
}
