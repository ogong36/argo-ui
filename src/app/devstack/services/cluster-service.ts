import axios from 'axios';
const accessToken = localStorage.getItem('accessToken');
import {endpoint} from '../../devstack/classes/constants';
import {Cluster} from '../users/components/models';
const headers = {
   Authorization: `Bearer ${accessToken}` 
}
export class ClusterService {
    public async get(): Promise<any> {
        try {
            const response = await axios.get(`${endpoint}/cluster`, { headers });
            return response.data;
        } catch(err) {
            return [];
        }
    }
    public async getDetail(id: string): Promise<any> {
        try {
            const response = await axios.get(`${endpoint}/cluster/${id}`, { headers });
            return response.data;
        } catch(err) {
            return [];
        }
    }
    public async create(data: any): Promise<any> {
        try {
            const response = await axios.post(`${endpoint}/cluster`, data, {headers});
            return response.data;
        } catch(err) {
            return { status: 'failure' };
        }
    }
    public async delete(data: Cluster): Promise<any> {
        try {
            const response = await axios.delete(`${endpoint}/cluster/${data.id}`, {headers});
            return { status: 'success', message: 'namespace deleted' }
        } catch(err) {
            return { status: 'failure' };
        }
    }
}
