import * as React from 'react';
import {Route, RouteComponentProps, Switch} from 'react-router';
import {UsersOverview,listUrl,roleUrl,namespaceUrl,clusterUrl} from './users-overview';
import {UsersNamespaces} from './namespaces/namespaces-list';
import {UsersList} from './users-list/users-list';
import {ClustersList} from './clusters/clusters-list';
import {UsersRoles} from './roles/roles-list';

export const UsersContainer = (props: RouteComponentProps<any>) => (
    <Switch>
        <Route exact={true} path={`${props.match.path}`} component={UsersOverview} />
        <Route exact={true} path={`${props.match.path}/${listUrl}`} component={UsersList} />
        <Route exact={true} path={`${props.match.path}/${roleUrl}`} component={UsersRoles} />
        <Route exact={true} path={`${props.match.path}/${namespaceUrl}`} component={UsersNamespaces} />
        <Route exact={true} path={`${props.match.path}/${clusterUrl}`} component={ClustersList} />
        {/* <Redirect path='*' to={`${props.match.path}`} /> */}
    </Switch>
);