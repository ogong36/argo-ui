import * as React from 'react';
import {Page} from 'argo-ui';
require('./menu.scss');
import {uiUrl} from '../../../../shared/base';
import {usersUrl} from '../../../classes/constants';
import {Link} from 'react-router-dom';
export const listUrl='list';
export const namespaceUrl='namespaces';
export const roleUrl='roles';
export const clusterUrl='cluster';
const menu = [
  {
      title: 'Manage User',
      description: 'Create/update/delete User',
      path: listUrl
  },
  {
      title: 'Manage Namespaces',
      description: 'Create/update/delete Namespaces',
      path: namespaceUrl
  },
  {
      title: 'Manage Role',
      description: 'View Roles',
      path: roleUrl
  },
  {
      title: 'Manage Cluster',
      description: 'View clusters',
      path: clusterUrl
  },
  // {
  //     title: 'Manage Tenant',
  //     description: 'Create/update/delete Tenant',
  //     path: 'tenants'
  // }
]

export const UsersOverview = () => {
  return (
    <Page title='User management' toolbar={{breadcrumbs: [{title: 'User management'}]}}>
      <div className='settings-overview'>
        <div className='argo-container'>
          {menu.map(item => (
              <Link to={usersUrl+'/'+item.path}>
                <div key={item.path} className='settings-overview__redirect-panel'>
                    <div className='settings-overview__redirect-panel__content'>
                        <div className='settings-overview__redirect-panel__title'>{item.title}</div>
                        <div className='settings-overview__redirect-panel__description'>{item.description}</div>
                    </div>
                    <div className='settings-overview__redirect-panel__arrow'>
                        <i className='fa fa-angle-right' />
                    </div>
                </div>
              </Link>
            ))}
        </div>
      </div>
    </Page>
  );
}
