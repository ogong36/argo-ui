import * as React from 'react';
import {Form} from 'react-bootstrap';
import { useState, useEffect } from 'react';
import {UserService} from '../../../../../services/user-service';
import {NamespaceService} from '../../../../../services/namespace-service';
import {RolesService} from '../../../../../services/roles-service';
import {Consumer, ContextApis} from '../../../../../../shared/context';
import { Select } from 'argo-ui';
import { Row, Col } from 'react-bootstrap';
import { history } from '../../../../../../app';
// import { User } from '../../../../components/models';

interface UserForm {
  name: string;
  email?: string;
  password: string;
  description?: string;
  primary_namespace_id: string;
  role_ids: string[];
  enabled?: boolean;
}

export default () => {
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [password2, setPassword2] = useState('');
  const [email, setEmail] = useState('');
  const [description, setDescription] = useState('');
  const [project, setProject] = useState('');
  const [roleName, setRoleName] = useState('');
  const [enabled, setEnabled] = useState(false);
  const [options, setOptions] = useState([]);
  const [roleOptions, setRoleOptions] = useState([]);
  const [users, setUsers] = useState([]);
  // const [user, setUser] = useState();
  const service = new UserService();
  const nsService = new NamespaceService();
  const roleService = new RolesService();
  
  
  const CheckEmail = (str)=>{
      var reg_email = /^([0-9a-zA-Z_\.-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,2}$/;
      if(!reg_email.test(str)) {
            return false;
        }
        else{
            return true;
       }
  }

  useEffect(() => {
      nsService.get().then(ns => {
        setOptions(ns.map((item: any) => { 
            return {title: item.name, value: item.id };
        }));
        setProject(ns[0].id);
      })
      roleService.get().then(r => {
        const v = r.map((item: {name: string;}) => item.name);
        setRoleOptions(r.map((item: any)=> {
            return {title:item.name, value:item.id};
        }));
        setRoleName(r[0].id);
      })
      service.getUsers().then(r =>{
          console.log(r);
        setUsers(r);
      })
  }, [])
  const handleSubmit = (ctx:ContextApis) => async (e: any) => {
      e.preventDefault();
      const userCheck = users.find(item=>{
          return item.name === name;
      });
      if(userCheck)
      {
          alert("user that already exists.");
          return;
      }
      if(password == '' || password2 == '')
      {
        alert("password is blank.");
        return;
      }
      if(password !== password2)
      {
        alert("password and conform password not same.");
        return;
      }
      if(!CheckEmail(email)){
        alert("email is invalid.");
        return;
      }
      if(project === ''){
        alert("select primary namespace.");
        return;
      }
      if(roleName === ''){
        alert("select role.");
        return;
      }
      const newUser: UserForm = {
          name,
          email,
          password,
          description,
          primary_namespace_id: project,
          role_ids: [roleName],
          enabled
      }
      const result = await service.register(newUser);
      if (result.status === 'success') {
          // post new user to backend
          clear();
          ctx.navigation.goto('.', {new: null,reload: true});
      } else {
          alert(`User Create Error ${name} ${password}`);
      }
  }
  const clear = ()=>{
    setName('');
    setPassword('');
    setPassword2('');
    setEmail('');
    setDescription('');
    setProject('');
    setRoleName('');
    setEnabled(false);
  }
  return (
    <Consumer>
        {ctx => (
            <form onSubmit={ handleSubmit(ctx) }>
                <div className='login__form-row'>
                    <button className='argo-button argo-button--base' type='submit'>
                        <i className='fa fa-plus-circle' /> Submit
                    </button>
                    <button className='argo-button argo-button--base' type='button' onClick={() =>{clear(); ctx.navigation.goto('.', {new: null})}}>
                        <i className='fa fa-times-circle' /> Cancel
                    </button>
                </div>
                <div className='argo-form-row'>
                    <Form.Group as={Row} controlId='formBasicUsername'>
                        <Form.Label column={true} sm={2}>User Name*</Form.Label>
                        <Col sm={10}>
                            <Form.Control type='text' placeholder='Enter username'
                                value={ name }
                                onChange={(e: { target: { value: React.SetStateAction<string>; }; }) => setName(e.target.value)}/>
                        </Col>
                    </Form.Group>
                </div>
                <div className='argo-form-row'>
                    <Form.Group as={Row} controlId='formBasicDescription'>
                        <Form.Label column={true} sm={2}>Description</Form.Label>
                        <Col sm={10}>
                            <Form.Control as='textarea' placeholder='Description'
                                value={ description }
                                onChange={(e: { target: { value: React.SetStateAction<string>; }; }) => setDescription(e.target.value)}/>
                        </Col>
                    </Form.Group>
                </div>
                <div className='argo-form-row'>
                    <Form.Group as={Row} controlId='formBasicEmail'>
                        <Form.Label column={true} sm={2}>Email</Form.Label>
                        <Col sm={10}>
                            <Form.Control type='email' placeholder='Email'
                                value={ email }
                                onChange={(e: { target: { value: React.SetStateAction<string>; }; }) => setEmail(e.target.value)}/>
                        </Col>
                    </Form.Group>
                </div>
                <div className='argo-form-row'>
                    <Form.Group as={Row} controlId='formBasicPassword1'>
                        <Form.Label column={true} sm={2}>Password</Form.Label>
                        <Col sm={10}>
                            <Form.Control type='password' placeholder='password'
                                value={ password }
                                onChange={(e: { target: { value: React.SetStateAction<string>; }; }) => setPassword(e.target.value)}/>
                        </Col>
                    </Form.Group>
                </div>
                <div className='argo-form-row'>
                    <Form.Group as={Row} controlId='formBasicPassword2'>
                        <Form.Label column={true} sm={2}>Confirm Password</Form.Label>
                        <Col sm={10}>
                            <Form.Control type='password' placeholder='confirm password'
                                value={ password2 }
                                onChange={(e: { target: { value: React.SetStateAction<string>; }; }) => setPassword2(e.target.value)}/>
                        </Col>
                    </Form.Group>
                </div>
                <div className='argo-form-row'>
                    <Form.Group as={Row} controlId='formBasicEnabled'>
                        <Form.Label column={true} sm={2}>Primary Project</Form.Label>
                        <Col sm={4}>
                            <Select options={options}
                                value={project}
                                onChange={(option) => setProject(option.value)}
                                placeholder='Select Service'
                            />
                        </Col>
                        <Form.Label column={true} sm={2}>Role</Form.Label>
                        <Col sm={4}>
                            <Select options= {roleOptions}
                                value={roleName}
                                onChange={(option) => setRoleName(option.value)}
                                placeholder='Select Role'
                            />
                        </Col>
                    </Form.Group>
                </div>
                <div className='argo-form-row'>
                    <Form.Group as={Row} controlId='formBasicEnabled'>
                        <Form.Label column={true} sm={2}>Enabled</Form.Label>
                        <Col sm={2}>
                            <Form.Check inline={true} type={'checkbox'}
                                defaultChecked={enabled}
                                value={enabled}
                                onChange={(e: { target: { value: React.SetStateAction<boolean>, checked: any }; }) => 
                                    {
                                        setEnabled(e.target.checked)
                                    }
                                }/>
                        </Col>
                    </Form.Group>
                </div>
            </form>
        )}
    </Consumer>
  );
}

