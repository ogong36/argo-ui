import * as React from 'react';
import {Loading} from '../../../../../../shared/components/loading';
import { Namespace } from '../../../models';
import {NamespaceService} from '../../../../../services/namespace-service';
import {AppContext, Consumer} from '../../../../../../shared/context';
import { Form, Row, Col } from 'react-bootstrap';
// import {MultiSelect} from './multiselector';
import Select from 'react-select';
import Axios from 'axios';
import { namespaceUrl } from '../../../users-overview';
import {NotificationType} from 'argo-ui';

import {UserService} from '../../../../../services/user-service';

import * as PropTypes from 'prop-types';
//import ListBox from 'react-listbox';
// import 'react-listbox/public/app.css';
// import 'react-listbox/public/ionicons.css';
// import 'react-listbox/dist/react-listbox.css';

//import {DualListBox} from 'react-dual-listbox';
//import 'react-dual-listbox/lib/react-dual-listbox.css';
//require('react-dual-listbox/src/scss/react-dual-listbox.scss');

import { string } from 'prop-types';

require('./workflow-drawer.scss');

const namespaceService = new NamespaceService();
const userService = new UserService();

interface NamespaceDrawerProps {
    name: string;
    domain_id: string;
    enabled?: boolean;
    id: string;
    is_domain?: boolean;
    parent_id?: string;
    k8s_ns?: string;
    quota_cpu?: number;
    quota_mem?: number; 
    onChange: (key: string) => void;
}

interface listMember {label: string,value: string}
interface NamespaceDrawerState {
    namespace?: Namespace;
    // userOptions?: any[];
    selectedUsers?: any[];
    selectedRole?: any[];
    selected?: string;
    roles?: listMember[];
    notIncludedMembers?: listMember[];
    includedMembers?: listMember[];
    resUserList?: any[];
}

const userOptions = [
    {label: 'user1', value: 'user1'},
    {label: 'user2', value: 'user2'},
    {label: 'user3', value: 'user3'},
 ];
 const roleOptions = [
     { label: 'wf-app-admin', value: 'wf-app-admin' },
     { label: 'executor', value: 'executor' },
     { label: 'viewer', value: 'viewer'}
 ];

//let resUserList: any[];
let currentRoleUserList=new Array<any>();
let allUserList = new Array<any>();
let selectedUserList = new Array<any>();
let nowSelectRole : any;
let nowNamespace : any;
let nowSelectedMember : any;
export class NamespaceDrawer extends React.Component<NamespaceDrawerProps, NamespaceDrawerState> {
    public static contextTypes = {
        router: PropTypes.object,
        apis: PropTypes.object
    };

    constructor(props: NamespaceDrawerProps) {
        super(props);
        this.state = {};
    }

    public cancelDrawer() {

    }
    public getRoleList(){
        Axios.get('/role')
        .then(res =>{
            let roleList=new Array<listMember>();
            const val = res.data as any[]
            val.map(data => {
                roleList.push({label:data.name,value:data.id})
            });
            this.setState({roles:roleList})
        });
    }
    
    public getMemberList(){
        Axios.get('namespace/'+this.state.namespace.id+'/member')
        .then(res=>{
            //resUserList=res.data;
            this.setState({resUserList:res.data})
        });
    }
    public setMemberRole(selectedValues){
        currentRoleUserList.map(item =>{
            let value = {add: [],remove: []};
            if(0<=selectedValues.indexOf(item.value))
            {
                value.add.push({id:item.value,roles:[nowSelectRole.value]});
            }
            else{
                value.remove.push({id:item.value,roles:[nowSelectRole.value]});
            }
            Axios.patch('namespace/'+nowNamespace.id+'/member',value);
        });
        //
        
      // handle selected values here
    };

    public addMemberRole(){
        if(nowSelectedMember)
        {
            let value = {add: [],remove: []};
            value.add.push({id:nowSelectedMember.value,roles:[nowSelectRole.value]});
            Axios.patch('namespace/'+nowNamespace.id+'/member',value);
            this.appContext.apis.notifications.show({
                content: 'add '+nowSelectRole.label+ 'role in' + nowSelectedMember.label,
                type: NotificationType.Success
            });
        }
        //
        this.getMemberList();
      // handle selected values here
    };
    public deleteMemberRole(){
        if(nowSelectedMember)
        {
            let value = {add: [],remove: []};
            value.remove.push({id:nowSelectedMember.value,roles:[nowSelectRole.value]});
            Axios.patch('namespace/'+nowNamespace.id+'/member',value);
            this.appContext.apis.notifications.show({
                content: 'delete '+nowSelectRole.label+ 'role in' + nowSelectedMember.label,
                type: NotificationType.Success
            });
        }
        this.getMemberList();
    }


    public selectMember(selectedValues){
        nowSelectedMember=selectedValues;
    }

    public componentDidMount() {
        namespaceService.getProfile(this.props.id).then(namespace=> {
            const flatten = Object.assign(
                {}, 
                ...function _flatten(o): any { 
                  return [].concat(...Object.keys(o)
                    .map(k => 
                      typeof o[k] === 'object' ?
                        _flatten(o[k]) : 
                        ({[k]: o[k]})
                    )
                  );
                }(namespace)
              )
            nowNamespace = flatten;
            this.setState({namespace: flatten});
            this.getMemberList();
            this.getRoleList();
            
        });
        // this.setState({userOptions: [
        //     {label: 'user1', value: 'user1'},
        //     {label: 'user2', value: 'user2'},
        //     {label: 'user3', value: 'user3'},
        //  ]})
        this.setState({selectedUsers: []})
        this.setState({selectedRole: []})
    }

    public changeHandler = (key: string) => (e: any) => {
        this.setState({
            namespace: {
                ...this.state.namespace,
                [key]: e.target.value
            }
        })
    }
    
    public selectHandler = (selectedOption: any) => {
        
        (selectedUserList as any[]).length =0;
        (currentRoleUserList as any[]).length =0;
        allUserList.length=0;
        this.state.resUserList.map(item =>{
            allUserList.push({label:item.name,value:item.id});
            if(item.roles[0]){
                
                if (item.roles[0].id == selectedOption.value){
                    currentRoleUserList.push({label:item.name,value:item.id});
                    selectedUserList.push(item.id);
                }
            }
            else{
                currentRoleUserList.push({label:item.name,value:item.id});
            }
        });
        //includedMembers
        nowSelectRole = selectedOption;

        const selected = this.state.selectedUsers.concat(selectedOption.value);
        // this.setState({selectedUsers: selected})
        this.setState(
            { selectedUsers: [...this.state.selectedUsers, selectedOption.value] }
        );
        this.setState(
            { selected: selected + ', ' + selectedOption.value }
        );
        // this.setState({ selectedUsers: selected });
    }

    public submitHandler = async (e: any) => {
        e.preventDefault();

        /******  update payload에 싣기 위해서 NamespaceForm type을 맞춰서 보낸다. *****/

        // const res = await namespaceService.update(this.state.namespace);
        // if (res.status === 'success') {
        //     alert('Namespace updated');
        // } else {
        //     alert('Namespace update failed')
        // }
    }

    public render() {
        if (!this.state.namespace) {
            return <Loading />;
        }
        
        return (
            <Consumer>
                {ctx => (
                    <div className='workflow-drawer'>
                        <table>
                            <tbody>
                                <tr>
                                    <th>Namespace Name</th>
                                    <td>{this.state.namespace.name}</td>
                                </tr>
                                <tr>
                                    <th>Namespace ID</th>
                                    <td>{this.state.namespace.id}</td>
                                </tr>
                                <tr>
                                    <th>Domain ID</th>
                                    <td>{this.state.namespace.domain_id}</td>
                                </tr>
                                <tr>
                                    <th>Enabled</th>
                                    <td>{this.state.namespace.enabled ? 'Y' : 'N'}</td>
                                </tr>
                                {/* <tr>
                                    {Object.entries(this.state.namespace.wf).map(([key, value]) => {
                                        return (
                                        <><tr><th>{key}</th>
                                                <td>{value}</td></tr></>
                                        )
                                    })}
                                </tr> */}
                                <tr>
                                    <th>Quota</th><td/>
                                </tr>
                                <tr>
                                    <th>CPU</th>
                                    <td>{this.state.namespace.quota_cpu}</td>
                                </tr>
                                <tr>
                                    <th>Memory</th>
                                    <td>{this.state.namespace.quota_mem}</td>
                                </tr>
                                <tr>
                                    <th>Description</th>
                                    <td>{this.state.namespace.description}</td>
                                </tr>
                            </tbody>
                        </table>
                        <form onSubmit={this.submitHandler}>
                            <div className='login__form-row'>
                                <button className='argo-button argo-button--base' type='submit'>
                                    <i className='fa fa-plus-circle' /> Submit
                                </button>
                                {/* <button className='argo-button argo-button--base' type='button' onClick={this.cancelDrawer}>
                                    <i className='fa fa-times-circle' /> Cancel
                                </button> */}
                            </div>
                            <div className='argo-form-row'>
                                <Form.Group as={Row} controlId='formBasicName'>
                                    <Form.Label column={true} sm={2}>Name*</Form.Label>
                                    <Col sm={10}>
                                        <Form.Control type='text' placeholder='Enter namespace name'
                                            value={ this.state.namespace.name }
                                            onChange={this.changeHandler('name')}
                                        />
                                    </Col>
                                </Form.Group>
                            </div>
                            <div className='argo-form-row'>
                                <Form.Group as={Row} controlId='formBasicRole'>
                                    <Form.Label column={true} sm={2}>Role</Form.Label>
                                    <Col sm={10}>
                                        <Select options={this.state.roles}
                                            onChange={this.selectHandler} />
                                    </Col>
                                </Form.Group>
                            </div>
                            
                            <div className='argo-form-row'>
                                <Form.Group as={Row} controlId='formBasicRole'>
                                    <Form.Label column={true} sm={2}>Users</Form.Label>
                                    <Col sm={10}>
                                        <Select options={allUserList}
                                            onChange={this.selectMember} />
                                    </Col>
                                </Form.Group>
                                <button className='argo-button argo-button--base' type='button' onClick={this.addMemberRole.bind(this)}>
                                    <i className='fa fa-plus-circle' /> Role Add
                                </button>
                                <button className='argo-button argo-button--base' type='button' onClick={this.deleteMemberRole.bind(this)}>
                                    <i className='fa fa-minus-circle' /> Role Remove
                                </button>
                                <table>
                                    <tr>
                                    <th>name</th>
                                    <th>role</th>
                                    </tr>
                                {this.state.resUserList && this.state.resUserList.map(item=>{
                                    return <tr><td> {item.name}</td><td>{(item.roles[0]?item.roles[0].name:"no role")}</td></tr>
                                })}
                                </table>
                            </div>
                            {/* <div className='argo-form-row'>
                                {this.state.selected &&(<DualListBox
                                        options ={currentRoleUserList}
                                        selected = {selectedUserList}
                                        onChange = {this.setMemberRole}
                                    />)}
                            </div> */}
                            {/* <div className='argo-form-row'>
                                { 
                                    Object.entries(this.state.namespace.wf).map(([key, value]) => {
                                        return (
                                            // tslint:disable-next-line:jsx-key
                                            <Form.Group as={Row} controlId='formBasicCpu'>
                                                <Form.Label column={true} sm={2}>{key}</Form.Label>
                                                <Col sm={10}>
                                                    <Form.Control type='number' 
                                                        name={key}
                                                        value={value}
                                                        onChange={this.changeHandler(key)}
                                                    />
                                                </Col>
                                            </Form.Group>
                                        )
                                    })
                                }
                            </div> */}
                            <div className='argo-form-row'>
                                <Form.Group as={Row} controlId='formBasicCpu'>
                                    <Form.Label column={true} sm={2}>CPU</Form.Label>
                                    <Col sm={10}>
                                        <Form.Control type='number' 
                                            value={this.state.namespace.quota_cpu}
                                            onChange={this.changeHandler('quota_cpu')}
                                        />
                                    </Col>
                                </Form.Group>
                            </div>
                            <div className='argo-form-row'>
                                <Form.Group as={Row} controlId='formBasicMemory'>
                                    <Form.Label column={true} sm={2}>Memory</Form.Label>
                                    <Col sm={10}>
                                        <Form.Control type='number' 
                                            value={this.state.namespace.quota_mem}
                                            onChange={this.changeHandler('quota_mem')}
                                        />
                                    </Col>
                                </Form.Group>
                            </div>

                            <div className='argo-form-row'>
                                <Form.Group as={Row} controlId='formBasicEnabled'>
                                    <Form.Label column={true} sm={2}>Enabled</Form.Label>
                                    <Col sm={2}>
                                        <Form.Check inline={true} type={'checkbox'}
                                            defaultChecked={this.state.namespace.enabled}
                                            value={ this.state.namespace.enabled } 
                                            onChange={this.changeHandler('enabled')}
                                        />
                                    </Col>
                                </Form.Group>
                            </div>
                        </form>
                    </div>
                )}
            </Consumer>
          );
    }
    private get appContext(): AppContext {
        return this.context as AppContext;
    }
}
