import * as React from 'react';
import { Page } from 'argo-ui';
require('./notfound.scss');


export default () => {
    
    return (
        <Page title='notfound'>
            <div className='row'>
                에러코드 404<br/>
                페이지를 찾을 수 없습니다.<br/>
            </div>
        </Page>
    );
};