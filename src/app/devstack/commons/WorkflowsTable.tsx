import React = require('react');
import { Table } from 'react-bootstrap';
import {IWorkflow} from '../overview/components/overview';

export const WorkflowsTable = ((workflows: IWorkflow[]) => {
  const include = ['name', 'phase', 'startedAt', 'finishedAt', 'nodeDurationFormatted', 
                  'resourceDurationCPU', 'resourceDurationMem','clusterName','namespace'];
  return (
    <Table striped={true} bordered={true} hover={true} size={'sm'}>
      <thead>
        <tr>
              <th className='columns small-1'>NAME</th>
              <th className='columns small-1'>Phase</th>
              <th className='columns small-1'>Started At</th>
              <th className='columns small-1'>Finished At</th>
              <th className='columns small-1'>Node Duration</th>
              <th className='columns small-1'>CPU</th>
              <th className='columns small-1'>Memory</th>
              <th className='columns small-1'>Cluster</th>
              <th className='columns small-1'>Namespace</th>
        </tr>
      </thead>
      <tbody>
          { workflows.map(workflow => {
            return (
              // tslint:disable-next-line:jsx-key
              <tr key={workflow.uid}>
                { 
                  include.map((key) => {
                    return (
                      // tslint:disable-next-line:jsx-key
                      <td key={key}>
                        { workflow[key] }
                      </td>
                    )
                  })
                }
              </tr>
            );
          })}
      </tbody>
    </Table>
  )
});