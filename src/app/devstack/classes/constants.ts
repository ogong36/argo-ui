import {uiUrl} from '../../shared/base';
const workflowsUrl = uiUrl('workflows');
const workflowTemplatesUrl = uiUrl('workflow-templates');
const clusterWorkflowTemplatesUrl = uiUrl('cluster-workflow-templates');
const cronWorkflowsUrl = uiUrl('cron-workflows');
const archivedWorkflowsUrl = uiUrl('archived-workflows');
export const helpPath = 'help';
export const apiDocsPath = 'apidocs';
export const userInfoPath = 'userinfo';
export const loginPath = 'login';
export const logoutPath = 'logout';
export const registerPath = 'register';
export const timelinePath = 'timeline';
export const reportsPath = 'reports';
export const usersPath = 'user-manager';
export const overviewPath = 'overview';
export const metricPath = 'metric';
export const monitorPath = 'monitor';
export const meteringPath = 'metering';

export const helpUrl = uiUrl(helpPath);
export const apiDocsUrl = uiUrl(apiDocsPath);
export const userInfoUrl = uiUrl(userInfoPath);
export const loginUrl = uiUrl(loginPath);
export const logoutUrl = uiUrl(logoutPath);
export const registerUrl = uiUrl(registerPath);
export const timelineUrl = uiUrl(timelinePath);
export const reportsUrl = uiUrl(reportsPath);
export const usersUrl = uiUrl(usersPath);
export const overviewUrl = uiUrl(overviewPath);
export const metricUrl = uiUrl(metricPath);
export const monitorUrl = uiUrl(monitorPath);
export const meteringUrl = uiUrl(meteringPath);


//const isProd = process.env.NODE_ENV === 'production';
//export const endpoint = 'http://localhost:3000';
const PORT =  process.env.PORT || 3000;
const URL = process.env.URL || 'localhost';
export const endpoint = '';

export const navItems = {
  loggedInUser: [
      {
          title: 'Logout',
          path: logoutUrl,
          iconClassName: 'fa fa-sign-out-alt'
      }
  ],
  anonymousUser: [
      {
          title: 'Login',
          path: loginUrl,
          iconClassName: 'fa fa-sign-in-alt'
      }
  ],
  admin2: [
    {
        title: 'Overview',
        path: overviewUrl,
        iconClassName: 'fa fa-eye'
    }
  ],
  user: [
      {
          title: 'Timeline',
          path: workflowsUrl,
          iconClassName: 'fa fa-stream'
      },
      {
          title: 'Workflow Templates',
          path: workflowTemplatesUrl,
          iconClassName: 'fa fa-window-maximize'
      },
      {
          title: 'Cluster Workflow Templates',
          path: clusterWorkflowTemplatesUrl,
          iconClassName: 'fa fa-window-restore'
      },
      {
          title: 'Cron Workflows',
          path: cronWorkflowsUrl,
          iconClassName: 'fa fa-clock'
      },
      {
          title: 'Monitor',
          path: monitorUrl,
          iconClassName: 'fa fa-tachometer-alt'
      },
      {
          title: 'Metering',
          path: meteringUrl,
          iconClassName: 'fa fa-ruler-horizontal'
      }
  ],
  tadmin: [
      {
          title: 'User',
          path: usersUrl,
          iconClassName: 'fa fa-user-alt'
      }
  ],
  admin: [
      {
          title: 'Archived Workflows',
          path: archivedWorkflowsUrl,
          iconClassName: 'fa fa-archive'
      }
  ]
};
