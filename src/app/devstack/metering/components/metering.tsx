import { Table } from 'react-bootstrap';
import DatePicker, { DayValue, DayRange, Day } from 'react-modern-calendar-datepicker'
import 'react-modern-calendar-datepicker/lib/DatePicker.css';
// import DatePicker from 'react-datepicker'; 
import { addDays } from 'date-fns';
import { Page, Select } from 'argo-ui';
import * as React from 'react';
import { useState, useEffect } from 'react';
import { NamespaceService } from '../../services/namespace-service';
import { ClusterService } from '../../services/cluster-service';
import { MeteringService } from '../../services/metering-service';
import namespaces from '../../services/__mocks__/namespaces';
import {uiUrl} from '../../../shared/base';
import {Link} from 'react-router-dom';

const service = new NamespaceService();
const clusterService = new ClusterService();
const meteringService = new MeteringService();

interface cell {
  key: string;
  align : "left" | "right" | "center" | "justify" | "char";
}
export default () => {
  const [namespace, setNamespace] = useState('');
  const [options, setOptions] = useState([]);
  const [cluster, setCluster] = useState('');
  const [clusterOptions, setClusterOptions] = useState([]);
  const [workflows, setWorkflows] = useState([])
//   const [startDate, setStartDate] = useState(new Date());
//   const [endDate, setEndDate] = useState(null);
//   const onDateChange = (dates: any) => {
//     const [start, end] = dates;
//     setStartDate(start);
//     setEndDate(end);
//   };
    const today = new Date()
    const defaultFrom = {
        year: today.getFullYear(),
        month: today.getMonth() + 1,
        day: today.getDate() - 7
    }
    const defaultTo ={
        year: today.getFullYear(),
        month: today.getMonth() + 1,
        day: today.getDate()
    }
    const defaultRange = {
        from: defaultFrom,
        to: defaultTo,
      };
    

    const [dayRange, setDayRange] = React.useState<DayRange>(defaultRange);
    
    const getWorkflows = (startDate: string, endDate: string) => {
        meteringService.get(startDate, endDate).then((workflowsData: any) => {
          setWorkflows(workflowsData);
        });
    };

    useEffect(() => {
        service.get().then(ns => {
            setOptions(ns.map((item: {name: string; }) => item.name));
        });
        clusterService.get().then(clusters => {
            setClusterOptions(clusters.map((item: { name: string }) => item.name));
        });
        const start = (new Date(Date.UTC(dayRange.from.year, dayRange.from.month - 1, dayRange.from.day))).toISOString();
        const end = (new Date(Date.UTC(dayRange.to.year, dayRange.to.month - 1, dayRange.to.day+1))).toISOString();
        getWorkflows(start, end);
    }, [])

  const filter = (namespace,cluster)=>{
    const startDate = (new Date (Date.UTC(dayRange.from.year, dayRange.from.month - 1, dayRange.from.day))).toISOString();
    const endDate = (new Date(Date.UTC(dayRange.to.year, dayRange.to.month - 1, dayRange.to.day+1))).toISOString();
    meteringService.getByNamespace(namespace,startDate, endDate).then(workflowsData=>{
      if(cluster !== ''){
        const filteredWorkflows = workflowsData.filter((workflow) => {
          return workflow.clusterName === cluster;
        });
        setWorkflows(filteredWorkflows);
      }else{
        setWorkflows(workflowsData);
      }
    });
  }
  const changeNamespaceHandler = (value: string) => {
    setNamespace(value);
    filter(value,cluster);
  }

  const changeClusterHandler = (value: string) => {
    setCluster(value);
    filter(namespace,value);
  }

  const clickHandler = () => {
      const startDate = (new Date(Date.UTC(dayRange.from.year, dayRange.from.month - 1, dayRange.from.day))).toISOString();
      const endDate = (new Date(Date.UTC(dayRange.to.year, dayRange.to.month - 1, dayRange.to.day+1))).toISOString();
      getWorkflows(startDate, endDate);
  }
  let sum=0;
  return (
    <Page title='Metering'>
        <div className='row'>
          <div className='columns small-4'>
              <br/><br/>
                <DatePicker value={dayRange} onChange={setDayRange} />
                <button className='argo-button argo-button--base argo-button--sm'  onClick={clickHandler}>Go</button>
                {/* <DatePicker
                    selected={this.props.maxStartedAt}
                    onChange={date => {
                        this.props.onChange(this.props.namespace, this.props.cluster, this.props.selectedPhases, this.props.selectedLabels, this.props.minStartedAt, date);
                    }}
                    placeholderText='To'
                    dateFormat='dd MMM yyyy'
                    todayButton='Today'
                    className='argo-field argo-textarea'
                /> */}
          </div>
          <div className='columns small-4'>
            <form>
                <div className='argo-form-row'>
                    <Select options={options}
                        placeholder='Select Namespace'
                        value={namespace}
                        onChange={(option) => changeNamespaceHandler(option.value) } />
                    {(namespace !== '') && 
                        <a
                            onClick={() => {
                                setNamespace('');
                                changeNamespaceHandler('');
                            }}>
                            <i className='fa fa-times-circle' /> Clear selection
                        </a>
                    }
                </div>
            </form>
          </div>
          <div className='columns small-4'>
            <form>
                <div className='argo-form-row'>
                    <Select options={clusterOptions}
                    placeholder='Select Cluster'
                    value={cluster}
                    onChange={(option) => changeClusterHandler(option.value) } />
                    {(cluster !== '') && 
                        <a
                            onClick={() => {
                                setCluster('');
                                changeClusterHandler('');
                            }}>
                            <i className='fa fa-times-circle' /> Clear selection
                        </a>
                    }
                </div>
            </form>
          </div>
        </div>
     
        <Table striped={true} bordered={true} hover={true} size={'sm'}>
          <thead>
            <tr>
                  <th className='columns small-1'>NAME</th>
                  <th className='columns small-1'>Phase</th>
                  <th className='columns small-1'>Started At</th>
                  <th className='columns small-1'>Finished At</th>
                  <th className='columns small-1'>Node Duration</th>
                  <th className='columns small-1'>CPU</th>
                  <th className='columns small-1'>Memory</th>
                  <th className='columns small-1'>Cluster</th>
                  <th className='columns small-1'>Namespace</th>
                  <th className='columns small-1'>Cost</th>
            </tr>
          </thead>
          <tbody>
              {workflows.map((workflow: any) => {
                if(options.includes(workflow.namespace)){
                  const include = [
                    {key:'name',align:'left'}, 
                    {key:'phase',align:'left'}, 
                    {key:'startedAt',align:'left'}, 
                    {key:'finishedAt',align:'left'}, 
                    {key:'nodeDurationFormatted',align:'right'}, 
                    {key:'resourceDurationCPU',align:'right'}, 
                    {key:'resourceDurationMem',align:'right'},
                    {key:'clusterName',align:'left'},
                    {key:'namespace',align:'left'}
                  ];
                    
                  sum +=workflow.resourceDurationCPU*1667;
                  return (
                    // tslint:disable-next-line:jsx-key
                    
                    <tr key={workflow.uid}>
                      
                      { include.map((key:cell) => {
                          return (
                            // tslint:disable-next-line:jsx-key
                            <td align={key.align}>
                                { workflow[key.key] }
                            </td>
                          )
                        })
                      }
                      <td align={'right'}>
                          {workflow.resourceDurationCPU*1667} USD
                      </td>
                    </tr>
                    
                  );
                }
              })}
              <tr>
                <td colSpan={9}>
                  sum = 
                </td>
                <td align={'right'}>
                {sum} USD
                </td>
              </tr>
          </tbody>
        </Table>
    </Page>
  )
}